// Imprime "Hola mundo" en la consola
console.log("Hola mundo")

// Declaración de variables 
var s = "Foo Bar"

let x = 90
var y = 89

// Imprime el contenido de la variable s
console.log(s)
// Imprime la suma de x y y
console.log(x + y)

// Declara la variable s con un valor booleano
s = true

console.log(s)

// Definición de un array con diferentes tipos de datos.
ARRAYS
array = [1,2,3,4,5,"foo","bar",true,false,2.34,4.23]

//Impresion de diferentes maneras 
console.log(array)
console.log(array[5])

//OBJETOS(JASON)
//Se declara un objeto con diferentes propiedades
var obj = {
    firs_name: "Foo", 
    last_name: "Bar", 
    age: 23, 
    city: "TJ", 
    status: true,
    arr: array
}

//Impresion del objeto completo y impresion de diferentes propiedades
console.log(obj)
console.log(obj["firs_name"]) //por acceso
console.log(obj.last_name) //por invocacion(recorrer el objeto)


//LOOPS
//Imprime los numeros de 0 al 100 de 5 en 5
for(let i=0; i<100; i+=5) {
    console.log(i)
}

//Imprime los elementos del array
for(let i=0; i<array.length; i++) {
    console.log(array[i])
}
for(let i of array){
    console.log(i)
}

//Imprime propiedades y valores de un objeto.
for(let key of Object.keys(obj)){ //Mas directa para los arrays
    console.log(key + ": " + obj[key])
}
for(let key in obj) { //Mas directa para los objetos indica que clase y que metodo necesitas
    console.log(key + ": " + obj[key])
}

//While que cuenta hacia atras del 1000 al 0
var i = 1000
while(i <= 1000 && i >= 0) {
    console.log(i)
    i-=5
}

//eDo while que cuenta del 0 al 1000
var k = 0
do{
    console.log(k)
    k += 5
} while(k < 1000)

// Estructura condicional if
var x = 9
var y = 8
if(x > y){
    console.log("Es 1")
} else {
    console.log("Is Not")
}

//Estructura condicional switch
var opc = 8

switch(opc){
    case 1:
        console.log("I am the case 1")
    break;
    case 8: 
        console.log("I am the case 8")
    break;
    default:
        console.log("I am default")
    break;
}

// Se define una variable animal con el valor "chikis".
var animal = "chikis"
// Se utiliza === para verificar si 'animal' es igual a "chikis"
// Si es verdadero, se asigna "Is my little princess" a 'hello', de lo contrario, se asigna "Is not my princess"
var hello = (animal === "chikis") ? "Is my little princess" : "Is not my princess"
//imprime el mensaje
console.log(hello)

//Funcion para imprimir hello
function foo() {
    var c = "hello"
    console.log(c)
}
foo()

//Se declara la variable a con "hello"
var a = "hello"
//Al llamar a la función foo, se imprime en la consola el valor de la variable a, que es "word"
function foo(){
    var a = "word"
    function bar(){
        console.log(a)
    }
    bar()
}
foo()

//Funcion dentro de una variable 
var prism = function(l,w,h) {
    return l * w * h;
}
console.log("Volumen del prisma: "+prism(23,56,12))

//Funcion anidada
function prisma(l) {
    return function(w) { 
        return function(h) {
            return l*w*h;
        }
    }
}
console.log("Volumen del prisma2: "+prisma(2)(3)(5))

// Se define una función que toma un parámetro 'msg'.
// Imprime en la consola un saludo concatenado con el valor de 'msg'.
// Retorna el número 45.
var msg = "como estas?"
const foo = (function(msg) {
    console.log("Holaaaa, " + msg)
    return 45;
})(msg);
console.log(foo)

//funciones en variables
var namedSum = function sum (a,b) {
    return a + b;
}
console.log(namedSum(1,3))

var anomSum = function (a,b) {
    return a + b;
}
console.log(anomSum(1,4))

fuu();
function fuu () {
    console.log("bar");
}

// Función recursiva que saluda un número específico de veces
var say = function say(times) {
    say = undefined;
    if(times > 0) {
        console.log("Hello!");
        say(times - 1);
    }
}
var sayHello = say;
sayHello(2);

// Funcion que cambia el valor de una variable después de asignar la función.
var saysay = say;
say = "oops";

saysay(2);

function foo(msg) {
    console.log(msg)
}
foo()
