// Se define la URL de la API
var url = 'http://api.stackexchange.com/2.2/questions?site=stackoverflow&tagged=javascript';
// Se realiza una solicitud GET a la URL
var responseData = fetch(url).then(response => response.json());
// Se encuentra la respuesta obtenida JSON y se imprime información
responseData.then(({items, has_more, quota_max, quota_remaining}) => {
    for (var {title, score, owner, link, answer_count} of items) {
        console.log("Q: "+title+" owner: "+owner)
    }
})

// Se define la URL de la API
var url = 'https://jsonplaceholder.typicode.com/posts';
// Se realiza una solicitud GET a la URL utilizando la función fetch
//Imprime el nombre de usuario de cada post
fetch(url).then(response => response.json())
    .then(response => {
        for (const user in response) {
            console.log("username: "+response[user].username)
         }
    }
)

// Se realiza una solicitud POST a la URL
fetch(url, {
    method: "POST",
    headers: {
        "Content-type": "application/json"
    },
    // Se envía un objeto JSON con el título del nuevo post.
    body: JSON.stringify({
        title: "Arriba los chihuahuas",
    })
}
// Se imprime la respuesta JSON que contiene la información del nuevo post
).then(response => response.json())
    .then(Post => {
        for (const title in Post) {
            console.log(Post)
        }
    })