'use strict';

/**
 * cargas-combustible router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::cargas-combustible.cargas-combustible');
