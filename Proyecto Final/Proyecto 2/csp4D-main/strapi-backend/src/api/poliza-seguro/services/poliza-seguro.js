'use strict';

/**
 * poliza-seguro service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::poliza-seguro.poliza-seguro');
