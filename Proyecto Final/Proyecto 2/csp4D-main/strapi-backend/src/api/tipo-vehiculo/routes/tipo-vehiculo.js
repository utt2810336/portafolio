'use strict';

/**
 * tipo-vehiculo router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::tipo-vehiculo.tipo-vehiculo');
