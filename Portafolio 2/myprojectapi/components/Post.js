import { FlatList, View } from "react-native-web";

return(
<View>
    <FlatList>
        data = {response}
        renderItem = {({item}) =>(
            <View>
                <Text>Id Post: {item.id}</Text>
                <Text>Title Post: {item.title}</Text>
            </View>
        )}
    </FlatList>
</View>
);