import React, { useState, useEffect } from 'react';
import { StatusBar } from 'expo-status-bar';
import { ActivityIndicator, StyleSheet, Text, View } from 'react-native';

const url = "https://api.coindesk.com/v1/bpi/currentprice.json";

const ApiBTC = () => {
  
  const [response, setResponse] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    fetch(url)
      .then(res => res.json())
      .then((result) => {
        setIsLoading(false);
        setResponse(result);
      })
      .catch((error) => {
        setIsLoading(false);
        setError(error);
      });
  }, []);

  console.log(response);

  const getContent = () => {
    if (isLoading) {
      return (
        <View style={styles.container}>
          <Text style={styles.textSize}>Loading Data...</Text>
          <ActivityIndicator size="large" />
        </View>
      );
    }
    if (error) {
      return (
        <View style={styles.container}>
          <Text>Error: {error.message}</Text>
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <Text style={styles.textSize}>BTC to USD: ${response && response.bpi && response.bpi.USD.rate}</Text>
        <StatusBar style="auto" />
      </View>
    );
  };

  return getContent();
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textSize: {
    fontSize: 20
  },
});

export default ApiBTC;
