//imports
import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, FlatList, ActivityIndicator } from 'react-native';

const url = "https://pokeapi.deno.dev/pokemon"; //Api

export default function Pokedex() {

    const[data, setData] = useState(null);
    const[error, setError] = useState(null);
    const[isLoading, setIsLoading] = useState(true);

    useEffect(()=>{
        fetch(url)
            .then(response => response.json())
            .then((result) => {
                setIsLoading(false);
                setData(result);
            }, (error) => {
                setIsLoading(false);
                setError(error);
            })
    },[])

    console.log(data)

    const getContent = () => {
        if(isLoading){
            return(
                <View>
                    <Text style={styles.textProps}>Loading Data...</Text>
                    <ActivityIndicator size="large" color="pink" />
                 </View>
            )
        }
        if(error){
            return <Text style={styles.textProps}>Error: {error}</Text>
        }

        return(
            <View>
                <FlatList
                    data={data}  
                    renderItem={({item}) => (
                        <View>
                            <Image style={styles.image} source={{uri:item.imageUrl}} />
                            <Text>Name: {item.name}</Text>
                            <Text>Genus: {item.genus}</Text>
                            <Text>Types: {item.types}</Text>
                        </View>
                    )}  
                />
            </View>
        )
    } 

    return(
        <View>
            {getContent()}
        </View>
    )  
}


//Constante de estilos
const styles = StyleSheet.create({
  textProps: {
    fontSize: 32,
    fontFamily: 'Gill Sans',
  },
  image: {
    width: 100,
    height: 100,
  },
});