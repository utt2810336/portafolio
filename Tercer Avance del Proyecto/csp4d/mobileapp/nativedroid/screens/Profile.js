//LISTO

import React, { useState, useEffect, useRef } from 'react';
import { View, Text, Image, StyleSheet, Animated, TextInput, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Ionicons } from '@expo/vector-icons';
import { LinearGradient } from 'expo-linear-gradient';

const Profile = ({ navigation, route }) => {
  const [userData, setUserData] = useState(null);
  const [editableUsername, setEditableUsername] = useState('');
  const [isEditingUsername, setIsEditingUsername] = useState(false);
  const textInputRef = useRef();
  const fadeAnim = useState(new Animated.Value(0))[0];
  const ip = '34.94.10.37';

  useEffect(() => {
    const { token } = route.params || { token: null };

    if (!token) {
      console.error('Token de autenticación no proporcionado');
      return null;
    }

    const fetchUserData = async () => {
      try {
        const response = await fetch(`http://${ip}:1337/api/users/me?populate=*`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        if (response.ok) {
          const data = await response.json();
          setUserData(data);
          setEditableUsername(data.username);
          Animated.timing(
            fadeAnim,
            {
              toValue: 1,
              duration: 1000,
              useNativeDriver: true
            }
          ).start();
        } else {
          console.error('Error al obtener los datos del usuario');
        }
      } catch (error) {
        console.error('Error:', error);
      }
    };
    fetchUserData();
  }, [route.params.token, fadeAnim]);

  const handleLogout = async () => {
    try {
      await AsyncStorage.removeItem('token');
      navigation.navigate('Login');
    } catch (error) {
      console.error('Error al cerrar sesión:', error);
    }
  };

  const handleEditUsername = () => {
    setIsEditingUsername(true);
  };

  useEffect(() => {
    if (isEditingUsername) {
      textInputRef.current?.focus();
    }
  }, [isEditingUsername]);

  const handleChangeUsername = (newUsername) => {
    setEditableUsername(newUsername);
  };

  const handleSaveUsername = async () => {
    try {
      const response = await fetch(`http://34.94.10.37:1337/api/users/${userData.id}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${route.params.token}`,
        },
        body: JSON.stringify({
          username: editableUsername,
        }),
      });

      if (response.ok) {
        setIsEditingUsername(false);
      } else {
        console.error('Error al actualizar el usuario');
      }
    } catch (error) {
      console.error('Error:', error);
    }
  };

  function formatMyDate() {
    return new Date(userData.updatedAt).toLocaleDateString('en-GB');
  }

  const handleChangePassword = () => {
    const { token } = route.params ? route.params : { token: null };
    navigation.navigate('Cambio', { token: token });
};

  

  return (
    <Animated.View style={[styles.container, { opacity: fadeAnim }]}>
      <LinearGradient colors={['#7b92b2', '#fff']} style={styles.background}>
        {userData && (
          <>
            <View style={styles.profileContainer}>
              <Image source={{ uri: `http://34.94.10.37:1337${userData.profilephoto.url}` }} style={styles.profileImage} />
              <Text style={styles.name}>{userData.nombrePila} {userData.apPaterno} {userData.apMaterno}</Text>
            </View>
            <View style={styles.detailsContainer}>
              <View style={styles.section}>
                <Text style={[styles.label, { color: '#fff' }]}>Username:</Text>
                <View style={styles.sectionWithIcon}>
                  {isEditingUsername ? (
                    <TextInput
                      ref={textInputRef}
                      value={editableUsername}
                      onChangeText={handleChangeUsername}
                      style={[styles.text, { color: '#181a2a', paddingHorizontal: 10 }]}
                    />
                  ) : (
                    <Text style={[styles.text, { color: '#181a2a', paddingHorizontal: 10 }]}>{userData.username}</Text>
                  )}
                  {!isEditingUsername ? (
                    <TouchableOpacity onPress={handleEditUsername}>
                      <Ionicons name="create-outline" size={24} color="#FFF" />
                    </TouchableOpacity>
                  ) : (
                    <TouchableOpacity onPress={handleSaveUsername}>
                      <Ionicons name="checkmark-outline" size={24} color="#FFF" />
                    </TouchableOpacity>
                  )}
                </View>
              </View>
              <View style={styles.section}>
                <Text style={[styles.label, { color: '#fff', marginBottom: 10 }]}>Correo:</Text>
                <Text style={[styles.text, { color: '#181a2a' }]}>{userData.email}</Text>
              </View>
              <View style={styles.section}>
                <Text style={[styles.label, { color: '#fff', marginBottom: 10 }]}>Telefono:</Text>
                <Text style={[styles.text, { color: '#181a2a' }]}>{userData.numTel}</Text>
              </View>
              <View style={styles.section}>
                <Text style={[styles.label, { color: '#fff', marginBottom: 10 }]}>Rol:</Text>
                <Text style={[styles.text, { color: '#181a2a' }]}>{userData.role.name}</Text>
              </View>
              <View style={styles.section}>
                <Text style={[styles.label, { color: '#fff', marginBottom: 2 }]}>Ultima actualizacion:</Text>
                <Text style={[styles.text, { color: '#181a2a' }]}>
                  {formatMyDate()}
                </Text>
              </View>
            </View>
            <View style={styles.buttonContainer}>
              <TouchableOpacity onPress={handleLogout} style={styles.button}>
                <Text style={styles.buttonText}>Cerrar sesión</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={handleChangePassword} style={[styles.button, styles.changePasswordButton]}>
                <Text style={styles.buttonText}>Cambiar Contraseña</Text>
              </TouchableOpacity>
            </View>
          </>
        )}
      </LinearGradient>
    </Animated.View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  background: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  profileContainer: {
    alignItems: 'center',
    marginTop: 50,
    marginBottom: 20,
  },
  profileImage: {
    width: 150,
    height: 150,
    borderRadius: 75,
    marginBottom: 10,
  },
  name: {
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#fff',
    paddingBottom: 30,
  },
  detailsContainer: {
    paddingHorizontal: 20,
  },
  section: {
    display: 'flex',
    borderBottomWidth: 2,
    borderBottomColor: '#7b92b2',
    paddingBottom: 10,
    marginBottom: 10,
    flexDirection: 'row',
    alignItems: 'center',
    minWidth: '85%',
    justifyContent: 'space-between',
  },
  sectionWithIcon: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'justify-end',
  },
  label: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 5,
    textAlign: 'center',
    color: '#fff',
  },
  text: {
    fontSize: 16,
    textAlign: 'center',
    color: '#fff',
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 0,
    paddingHorizontal: 30,
    width: '100%',
  },
  button: {
    backgroundColor: '#7b92b2',
    paddingVertical: 10,
    borderRadius: 5,
    paddingHorizontal: 8,
  },
  buttonText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#fff',
    textAlign: 'center',
  },
  changePasswordButton: {
    marginLeft: 5,
  },
});

export default Profile;
