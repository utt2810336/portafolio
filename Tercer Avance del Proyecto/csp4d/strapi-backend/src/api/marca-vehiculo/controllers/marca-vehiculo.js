'use strict';

/**
 * marca-vehiculo controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::marca-vehiculo.marca-vehiculo');
