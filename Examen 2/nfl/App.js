import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { Image } from 'react-native-web';

import Index from './.expo/components/Index';
import imageNFL from './assets/nflLogo.png';

export default function App() {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Pendientes en la NFL</Text> 
      <Image source={imageNFL} style={styles.image} /> 
      <Text>Seleccione una opcion: </Text>
      <Index />
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F2F3F4',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 10, 
  },
  image: {
    width: 200,
    height: 100,
    resizeMode: 'contain', 
    marginTop: 10,
  },
});
