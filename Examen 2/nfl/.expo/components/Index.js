import React, { useState, useEffect } from "react";
import { View, Text, ActivityIndicator, FlatList, StyleSheet, Picker } from "react-native";

const url = 'http://jsonplaceholder.typicode.com/todos'; // API

export default function Index() {
    const [data, setData] = useState([]);
    const [error, setError] = useState(null);
    const [isLoading, setIsLoading] = useState(true);
    const [selectedOption, setSelectedOption] = useState("pendingIds"); // Estado para la opción seleccionada del menú

    useEffect(() => {
        fetch(url)
            .then(response => response.json())
            .then((result) => {
                setIsLoading(false);
                setData(result);
            })
            .catch((error) => {
                setIsLoading(false);
                setError(error.message);
            });
    }, []);

    const getPendingTodosIds = () => {
        return data.filter(todo => todo).map(todo => ({id: todo.id}));
    };

    const getPendingTodosIdsAndTitles = () => {
        return data.filter(todo => todo).map(todo => ({ id: todo.id, title: todo.title }));
    };

    const getPendingTodosUnresolved = () => {
        return data.filter(todo => !todo.completed).map(todo => ({ id: todo.id, title: todo.title }));
    };

    const getPendingTodosResolved = () => {
        return data.filter(todo => todo.completed).map(todo => ({ id: todo.id, title: todo.title }));
    };

    const getPendingTodosIdAndUserId = () => {
        return data.filter(todo => todo).map(todo => ({ id: todo.id, userId: todo.userId }));
    };

    const getCompletedTodosIdAndUserId = () => {
        return data.filter(todo => todo.completed).map(todo => ({ id: todo.id, userId: todo.userId }));
    };

    const getPendingTodosUnresolvedIdAndUserId = () => {
        return data.filter(todo => !todo.completed).map(todo => ({ id: todo.id, userId: todo.userId }));
    };

    const renderListItem = ({ item }) => (
        <View style={styles.row}>
            {Object.entries(item).map(([key, value]) => (
                <Text key={key} style={styles.cell}>{key}: {value}</Text>
            ))}
        </View>
    );

    const getContent = () => {
        let todos;
        switch (selectedOption) {
            case "pendingIds":
                todos = getPendingTodosIds();
            break;
            case "pendingIdsAndTitles":
                todos = getPendingTodosIdsAndTitles();
            break;
            case "pendingUnresolved":
                todos = getPendingTodosUnresolved();
            break;
            case "pendingResolved":
                todos = getPendingTodosResolved();
            break;
            case "pendingIdsAndUserId":
                todos = getPendingTodosIdAndUserId();
            break;
            case "completedIdsAndUserId":
                todos = getCompletedTodosIdAndUserId();
            break;
            case "pendingUnresolvedIdsAndUserId":
                todos = getPendingTodosUnresolvedIdAndUserId();
            break
            default:
                todos = [];
        }

        if (isLoading) {
            return (
                <View style={styles.centered}>
                    <Text style={styles.textProps}>Loading Data...</Text>
                    <ActivityIndicator size="large" color="pink" />
                </View>
            );
        }
        if (error) {
            return (
                <View style={styles.centered}>
                    <Text style={styles.textProps}>Error: {error}</Text>
                </View>
            );
        }
        return (
            <FlatList
                data={todos}
                keyExtractor={(item, index) => index.toString()}
                renderItem={renderListItem}
            />
        );
    };

    return (
        <View style={styles.container}>
            <Picker
                selectedValue={selectedOption}
                onValueChange={(itemValue, itemIndex) =>
                    setSelectedOption(itemValue)
                }>
                <Picker.Item label="Todos los pendientes con ID" value="pendingIds" />
                <Picker.Item label="Todos los pendientes con Titulo" value="pendingIdsAndTitles" />
                <Picker.Item label="Todos los pendientes sin resolver" value="pendingUnresolved" />
                <Picker.Item label="Todos los pendientes resueltos" value="pendingResolved" />
                <Picker.Item label="Todos los pendientes con userID" value="pendingIdsAndUserId" />
                <Picker.Item label="Todos los pendientes resueltos con userID" value="completedIdsAndUserId" />
                <Picker.Item label="Todos los pendientes sin resolver con userID" value="pendingUnresolvedIdsAndUserId" /> 
            </Picker>
            {getContent()}
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 10,
    },
    centered: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textProps: {
        fontSize: 16, 
        fontFamily: 'Gill Sans',
    },
    heading: {
        fontSize: 20, 
        fontWeight: 'bold',
        marginTop: 20,
        marginBottom: 10,
    },
    row: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: '#ccc',
        paddingVertical: 5, 
    },
    cell: {
        flex: 1,
        textAlign: 'center',
        fontSize: 14, 
    },
    tableContainer: {
        marginBottom: 20,
    },
    tableTitle: {
        fontSize: 18, 
        fontWeight: 'bold',
        marginBottom: 10,
        color: '#333',
    },
    table: {
        borderWidth: 1,
        borderColor: '#ccc',
        borderRadius: 10,
        overflow: 'hidden',
    },
    tableHeader: {
        flexDirection: 'row',
        backgroundColor: '#f0f0f0',
        borderBottomWidth: 1,
        borderBottomColor: '#ccc',
    },
    headerCell: {
        flex: 1,
        paddingVertical: 10,
        paddingHorizontal: 5,
        fontWeight: 'bold',
        textAlign: 'center',
    },
});